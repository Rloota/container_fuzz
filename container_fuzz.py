import docker
import pyradamsa

import os
import sys
import uuid
import shutil
import logging
import tarfile
import tempfile
import argparse
import multiprocessing
from time import sleep, time
from multiprocessing import Pool, TimeoutError


class Container:
    
    image: str = None 
    _detach: bool = True
    _tty: bool = True
    _entrypoint: str = "/bin/sh"
    _auto_remove: bool = False
    build_local: bool = False #True, build using dockerfile if image missing, else pull from dockerhub.
    container_obj: object = None
    #client: object = None

    try:
        client = docker.from_env()
        #client = docker.DockerClient(base_url='url')
    except docker.errors.DockerException: 
        print("Start docker dummy")
        logging.error("Docker not started.")

class FuzzTool(Container):
    #fuzz
    def __init__(self):
        try:
            self.rad = pyradamsa.Radamsa()
        except:
            print("pyradamsa not installed?")
            sys.exit()
        try:
            logging.basicConfig(filename='fuzz.log', filemode='w', level=logging.INFO)
        except:
            print("logger broke?")

    base_folder = os.getcwd()
    sample_type = None
    sample_path = None
    sample_contents = None
    fuzz_sample = None
    amount = 5
    fuzz_mode_time = False
    fuzz_duration = 10800 #3 hours
    
    def generate_sample(self):
        self.fuzz_sample = self.rad.fuzz(self.sample_contents)
    def read_sample(self):

        sample_dict = {
            "pdf":"/samples/pdf/general_test_file.pdf",
            "pdf_ioc":"/samples/pdf/ioc_test.pdf",
            "pdf_javascript":"/samples/pdf/javascript_test.pdf",
            "txt":"/samples/txt/hello.txt",
            "txt_base64":"/samples/txt/base64.txt",
            "apk":"/samples/android_apk/selendroid-test-app.apk",
            "jar":"/samples/android_apk/selendroid-test-app-dex2jar.jar",
            "image":"/samples/image/post-scam-sms.png",
            "java":"/samples/java/HexDump.class",
            "msoffice":"/samples/msoffice/testfile.docm",
            "pcap":"/samples/pcap/ping_localhost.pcap",
            "html":"/samples/html/pdfxray_lite_report.html",
            "bin":"/samples/compressed/tampered_sample.bin",
            "exe":"/samples/msdos/suspicious_dos_sample.exe",
            "docm":"/samples/msoffice/testfile.docm",
            "log":"/samples/log/access.log",
            "eml":"/samples/txt/attachments.eml",
            "source_c":"/samples/source/c/overflow.c",
            "amd64":"/samples/amd64/hello_world",
            "hexdump":"/samples/java/HexDump.class",
            "lua":"/samples/source/lua/while.lua",
            "memory":"/samples/memory/Win7SP1x86_23418.raw",
            "disk":"/samples/disks/7-ntfs-undel.dd",
            "doc":"/samples/msoffice/very_suspicious.doc",
            "csv":"/samples/txt/test.csv",
            "eml":"/samples/eml/sample_date_parsing.eml"     
        }

        self.sample_path=self.base_folder + sample_dict[self.sample_type]

        ofile = open(self.sample_path, 'rb')
        self.sample_contents = ofile.read()
        ofile.close()

    def store_sample(self, tempf, iteration, cmd, non_critical = False):

        # tempf, temp file location
        # tool,  name of the tool
        # iteration, number of fuzz iteration in pool
        # cmd, command that was run

        tool = self.image.replace('/','_') #turn e.g. cincan/binwalk into cincan_binwalk
        fuzzpath=os.path.join(self.base_folder,'fuzz_output')
        try:
            os.mkdir(fuzzpath)
        except FileExistsError:
            pass #do nothing
        fullpath=os.path.join(fuzzpath,tool+'_output')
        try:
            os.mkdir(fullpath)
        except FileExistsError:
            pass #do nothing

        if non_critical:
            name = os.path.join(fullpath,tool+"_non_critical_fuzz"+str(iteration)+"_"+str(uuid.uuid4()))
        else:
            name = os.path.join(fullpath,tool+"_fuzz"+str(iteration)+"_"+str(uuid.uuid4()))
        with open(name+'.log', "w") as myfile:
            myfile.write(str(cmd))
        t = shutil.copy(tempf.name, fuzzpath) #copy tempfile into permanent storage
        os.rename(t,name)




    def copy_to_container(self, src, dst):

        #copy_to_container(tempf.name, container.name+':'+tempf.name)
        #src = path to file
        #dst container name:path to file
        name, dst = dst.split(':')
        try:
            container_name = Container.client.containers.get(name)
        except Exception as e:
            print("Container not running?") 
            print(e)
            logging.error("Container not running? %e"%e)
            sys.exit()
        os.chdir(os.path.dirname(src))
        srcname = os.path.basename(src)
        tar = tarfile.open(src + '.tar', mode='w')
        try:
            tar.add(srcname)
        finally:
            tar.close()

        data = open(src + '.tar', 'rb').read()
        container_name.put_archive(os.path.dirname(dst), data)

    def clean_env(self):
        try:
            print(self.container_obj.name())
            self.container_obj.remove(force=True)
        except AttributeError:
            pass
    
    def pull_image(self, pull_image):
        #self.client.images.pull(self.image)
        logging.info("Pulling %s"%pull_image)
        self.client.images.pull(pull_image)


    def fuzz(self, _command, _sample_type, _image):
        #Read sample
        self.sample_type = _sample_type
        self.image = _image
        self.read_sample() #stores to self.sample_contents
        #toolname = self.image.replace('/','_') #replace / from name to _ for writing to file FIXME
        max_breaks = 50 # How many != 0 returns before stopping fuzz
        max_breaks_n = 0 # FIXME
        non_critical_err = 0
        n_store_non_critical_errors = 0
        store_non_critical_errors = 5 #How many non-critical errors to store
        #self.fuzz_duration = _fuzz_duration #duration of fuzz, default 3600 (3h or until cancelled)
        self_total_tests = 0

        #start container
        try:
            self.container_obj = Container.client.containers.create(
            self.image, 
            detach=self._detach, 
            tty=self._tty, 
            entrypoint=self._entrypoint, 
            auto_remove=self._auto_remove 
            )
            logging.info("Starting pool with container: %s"%(self.container_obj.name))
              
        except docker.errors.ImageNotFound as e:
            if self.build_local == True:
                print("FIXME")
                pass #todo
            else:
                try:
                    self.client.images.pull(self.image) #pull from dockerhub
                    print('pulling from dockerhub')
                except:
                    print("Unable to pull :(")        #TEST THIS
                    logging.error("Unable to pull image!")
                    #sys.exit()

        self.container_obj.start()
        #Create temp file containing fuzz sample

        if self.fuzz_mode_time == 0: #Mode for testing new tools, quick debugging etc.. prints output, stores nothing.
            print('DEBUG MODE')
            t1 = time()
            limit = t1 + 10
            i = 0
            import subprocess
            while True:
                with tempfile.NamedTemporaryFile() as tempf:
                    try:
                        t2 = time()
                        if limit < t2:
                            logging.info("Fuzz time limit reached.")
                            break
                        self.generate_sample() #writes fuzz sample to self.fuzz_sample
                        tempf.write(self.fuzz_sample)
                        tempf.seek(0)
                        subpvar = subprocess.run(["cat","%s"%tempf.name], stdout=subprocess.PIPE)
                        #Insert into container
                        self.copy_to_container(tempf.name, self.container_obj.name+':'+tempf.name)
                        #Run cmd inside container
                        full_command = _command + " " +tempf.name
                        #cmd = self.container_obj.exec_run(full_command, stderr=True, user="root") #Root user required to avoid inconsistent behavior, debug...
                        cmd = self.container_obj.exec_run(full_command, stderr=True) #Root user required to avoid inconsistent behavior, debug...
                        self.container_obj.exec_run("rm /tmp/*")

                        if cmd[0] == 0 or cmd[0] == 1 or cmd[0] == 2:
                            if i % 10 == 0:
                                logging.info("Fuzz %s OK"%(i))

                            try:
                                print(cmd[1].decode('utf-8'))
                            except UnicodeDecodeError:
                                print(cmd[1])
                        else:
                            print("Fuzzing tool:%s, fuzz %s. ERROR!! Exit code: %s, writing log and storing test case."%(self.image,i, cmd[0]))
                            try:
                                print(cmd[1].decode('utf-8'))
                            except UnicodeDecodeError:
                                print(cmd[1])
                            logging.info("Fuzzing tool:%s, fuzz %s. ERROR!! Exit code: %s, writing log and storing test case. Container: %s"%(self.image,i, cmd[0], self.container_obj.name))
                            
                            max_breaks_n += 1
                            if max_breaks == max_breaks_n:
                                print("max breaks reached")
                                logging.info("Max breaks reached with container %s"%(self.container_obj.name))
                                break
                    except KeyboardInterrupt:
                        print("Received Interrupt, cleaning env")
                        logging.info("KeyboardInterrupt! Cleaning env ...")
                        logging.info("Pool for tool %s done, ran %s tests, %s errors. Cleaning ..."%(self.image, i, max_breaks_n))
                        self.container_obj.remove(force=True)
                    tempf.flush()
                    i+=1        
        
        if self.fuzz_mode_time == 1:
            t1 = time()
            limit = t1 + self.fuzz_duration # 3 hours per tool
            i = 0
            while True:
                with tempfile.NamedTemporaryFile() as tempf:
                    try:
                        t2 = time()
                        if limit < t2:
                            logging.info("Fuzz time limit reached.")
                            break
                        self.generate_sample() #writes fuzz sample to self.fuzz_sample
                        tempf.write(self.fuzz_sample)
                        tempf.seek(0) #pointer to beginning, pdf and other files write null without it .. (??)
                        #Insert into container
                        self.copy_to_container(tempf.name, self.container_obj.name+':'+tempf.name)
                        #Run cmd inside container
                        full_command = _command + " " +tempf.name
                        #cmd = self.container_obj.exec_run(full_command, stderr=True, user="root") #Root user required to avoid inconsistent behavior, debug...
                        cmd = self.container_obj.exec_run(full_command, stderr=True)
                        self.container_obj.exec_run("rm %s"%tempf.name, user="root") #delete case after successful fuzz - minor performance impact.
                        if cmd[0] == 0:
                            if i % 250 == 0:
                                logging.info("Fuzz %s OK, non-critical errors: %s, errors: %s"%(i, non_critical_err, max_breaks_n))
                                #print("Fuzz %s OK!"%(i))
                            #print("Fuzzing tool:%s, fuzz %s OK, code: %s"%(self.image,i, cmd[0]))
                            #print(cmd)
                            pass

                        elif cmd[0] == 126: #restart bugged container
                            logging.error("Error 126, restarting container!")
                            self.container_obj.restart()
                        elif cmd[0] == 1 or cmd[0] == 2:
                            non_critical_err +=1
                            if i % 250 == 0: #sanity check, print logs every 250th fuzz
                                logging.info("Fuzz %s OK, non-critical errors: %s, CRITICAL errors: %s"%(i, non_critical_err, max_breaks_n))
                                if n_store_non_critical_errors < store_non_critical_errors:
                                    self.store_sample(tempf, i, cmd, non_critical=True) #store some non-ciritcal errors for debugging
                                    n_store_non_critical_errors +=1
                        else: # if error code not 0,1,2 or 126 - mark as critical store case.
                            print("Fuzzing tool:%s, fuzz %s. ERROR!! Exit code: %s, writing log and storing test case."%(self.image,i, cmd[0]))
                            logging.info("Fuzzing tool:%s, fuzz %s. ERROR!! Exit code: %s, writing log and storing test case. Container: %s"%(self.image,i, cmd[0], self.container_obj.name))
                            self.store_sample(tempf, i, cmd)
                            max_breaks_n += 1
                            if max_breaks == max_breaks_n:
                                print("max breaks reached")
                                logging.info("Max breaks reached with container %s"%(self.container_obj.name))
                                break                        

                    except KeyboardInterrupt:
                        #Clean environment in case ctrl+c is pushed
                        print("Received Interrupt, cleaning environment")
                        logging.info("KeyboardInterrupt! Cleaning environment ...")
                        logging.info("Pool for tool %s done, ran %s tests, non-critical errors: %s, CRITICAL errors: %s. Cleaning ..."%(self.image, i, non_critical_err, max_breaks_n))
                        #self.container_obj.remove(force=True)
                    tempf.flush()
                    i+=1
                

        #Clean env after n runs
        print("Pool for tool %s done, cleaning ..."%(self.image))
        logging.info("Pool for tool %s done, ran %s tests, non-critical errors: %s, CRITICAL errors: %s. Cleaning ..."%(self.image, i, non_critical_err, max_breaks_n))
        self.container_obj.remove(force=True)

if __name__ == '__main__':


    #  script tool mode fuzz_duration
    parser = argparse.ArgumentParser(description='Pick n to run function #FIXME')
    parser.add_argument('script', nargs='?', default=1)
    parser.add_argument('tool', nargs='?', default='binwalk')
    parser.add_argument('mode', nargs='?', default=1)
    parser.add_argument('fuzz_duration', nargs='?', default=10800)

    args = parser.parse_args()
    n = int(args.script)
    tool = args.tool

    timeout = 30
    timeout_long = 100
    tooldict = {
    "binwalk":["timeout %s binwalk"%timeout,"msoffice"],
    "pyocr":["timeout %s python3 /home/appuser/ocr.py"%timeout, "image"], #check out
    "pdfid":["timeout %s python /pdfid/pdfid.py"%timeout, "pdf"],
    "pdf-parser":["timeout %s python3 pdf-parser.py"%timeout, "pdf"],
    "pdfxray-lite":["timeout %s python /pdfxray_lite/pdfxray_lite.py -f"%timeout, "pdf"],
    "peepdf":["timeout %s python /peepdf/peepdf.py"%timeout, "pdf"],
    "peframe":["timeout %s peframe"%timeout, "exe"],
    "7zip":["timeout %s 7z a -t7z files.7z"%timeout, "java"],
    "apktool":["timeout -t %s sh /home/appuser/tool/apktool d -f"%timeout, "apk"],
    "cfr":["timeout %s java -jar /usr/bin/cfr.jar"%timeout, "jar"],
    "eml_parser":["timeout %s eml2json"%timeout, "eml"],
    "flawfinder":["timeout %s flawfinder"%timeout, "source_c"],
    "floss":["timeout %s floss -s"%timeout, "amd64"],
    "ghidra-decompiler":["timeout %s sh /ghidra/ghidra_decompiler.sh decompile"%timeout_long, "amd64"],
    "ioc_strings":["timeout %s iocstrings"%timeout, "pdf_ioc"],
    "clamav":["timeout %s /usr/bin/clamscan"%timeout_long, "exe"],
    "iocextract":["timeout %s iocextract --input"%timeout, "pdf_ioc"],
    "jadx":["timeout -t %s ./bin/jadx"%timeout, "jar"],
    "jd-cli":["timeout %s java -jar /usr/local/bin/jd-cli.jar -od decompiled"%timeout, "jar"],
    "jsunpack-n":["timeout %s jsunpackn.py -c /jsunpack-n/options.config -V"%timeout, "pdf_javascript"],
    "manalyze":["timeout %s manalyze"%timeout, "exe"],
    "oledump":["timeout %s /usr/local/bin/python /oledump/oledump.py -S -s 3"%timeout, "docm"],
    "output-standardizer":["timeout %s standardizer json -q -o fuzz -t pdfxray_lite -i"%timeout, "html"],
    "pastelyzer":["timeout %s pastelyzer"%timeout, "txt_base64"],
    "radare2":["timeout %s sh /radare2/entrypoint.sh r2"%timeout, "exe"], 
    "regripper":["timeout %s perl /regripper/rip.pl"%timeout, "exe"],
    "sleuthkit":["timeout %s fls -d"%timeout,"disk"],
    "sleuthkit":["timeout %s fsstat"%timeout,"disk"], 
    "sleuthkit":["timeout %s fsstat"%timeout,"disk"], 
    "snowman-decompile":["timeout %s /snowman-master/build/nocode/nocode"%timeout,"exe"],
    "ssdc":["timeout %s ssdc"%timeout, "disk"],
    "ssdeep":["timeout %s /ssdeep/ssdeep"%timeout, "apk"],
    "tshark":["timeout %s tshark -r"%timeout, "pcap"],
    "xsv":["timeout %s /xsv headers"%timeout, "csv"],
    "yara":["timeout %s yara -w '/rules/index.yar' "%timeout, "exe"],
    "zsteg":["timeout %s /zsteg/bin/zsteg -a"%timeout, "image"]
    }
    fuzz_obj = FuzzTool()
    fuzz_obj.fuzz_mode_time = int(args.mode)
    fuzz_obj.fuzz_duration = int(args.fuzz_duration)

    def helper(tool): #for multiprocessing, since it does not support multiple variables...
        fuzz_obj.fuzz(tooldict[tool][0], tooldict[tool][1], "cincan/"+tool)
    def non_cincan_helper(tool):
        fuzz_obj.fuzz(tooldict[tool][0], tooldict[tool][1], tool)
       


    print("Lets go!")
    # in main because Pool uses Pickle and Pickle requires the function to be defined at the top-level. Nested functions are not importable...
    if n == 1:
        #Fuzz all tools
        for i in tooldict:
            print("Fuzzing tool %s."%i)
            fuzz_obj.pull_image("cincan/"+i)
            #fuzz_obj.pull_image("quay.io/cincan/"+i)
            logging.info("***************************Fuzzing tool: %s. ***************************"%i)
            pool = multiprocessing.Pool()
            pool.map(helper, [i]*4)
            pool.close()
            print("--------------------------Fuzzing tool %s done.--------------------------"%i)
            logging.info("--------------------------Fuzzing tool %s done.--------------------------"%i)

    if n == 2:
        #Fuzz single tool
        print("Fuzzing single tool")
        print("Fuzzing tool %s."%tool)
        logging.info("***************************Fuzzing tool: %s. ***************************"%tool)
        fuzz_obj.pull_image("cincan/"+tool)
        pool = multiprocessing.Pool()
        pool.map(helper, [tool]*4)
        pool.close()
        #fuzz_obj.fuzz(tooldict[tool][0], tooldict[tool][1], "cincan/"+tool)
        print("Fuzzing done.")
    
    if n == 3:
        #Fuzz single non-cincan tool
        print("For non cincan containers")
        print("Fuzzing tool %s."%tool)
        logging.info("***************************Fuzzing tool: %s. ***************************"%tool)
        pool = multiprocessing.Pool()
        pool.map(non_cincan_helper, [tool]*4)
        pool.close()
        print("Fuzzing done.")

    
    if n == 4: #no multiprocessing for testing
        tool = "pdfid"
        print("Fuzzing tool, single core %s."%tool)
        logging.info("***************************Fuzzing tool: %s. ***************************"%tool)
        fuzz_obj.fuzz(tooldict[tool][0], tooldict[tool][1], "cincan/"+tool)
