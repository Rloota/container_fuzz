A fuzzing framework that I quickly built to fuzz containerized DFIR tools. Relies heavily on [Radamsa](https://gitlab.com/akihe/radamsa) and Docker API. 

Can in theory be used to fuzz test any containerized cmd-based software.

Steps to run (tested on Manjaro and Ubuntu 20):

1. Install pyradamsa (e.g. pip3 install pyradamsa)
2. Install [Docker](https://docs.docker.com/get-docker/)
3. Run code: python3 container_fuzz.py

## TODO: Clean up code, write more universal user interface, write more comprehensive documentation.