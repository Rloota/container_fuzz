import pstats

file = open('perf_output', 'w')
profile = pstats.Stats('.\profile_results', stream=file)
profile.sort_stats('cumulative') # Sorts the result according to the supplied criteria
profile.print_stats(15) # Prints the first 15 lines of the sorted report
file.close()