#!/bin/bash

#samples/msdos/suspicious_dos_sample.exe
#docker run -it --entrypoint "/bin/sh" cincan/clamav

#docker run -v $PWD/samples:/samples cincan/binwalk /samples/firmware.bin`

SAMPLE="/mnt/c/Projects/fuzz_osint/samples/msdos/suspicious_dos_sample.exe"

#start container and leave it running
docker run -t -d --entrypoint "/bin/sh" -it cincan/floss
container_ID=$(docker ps -q)
#generate samples
echo "starting!"
for run in {1..10}; 
    do
    echo "fuzz"
    #radamsa $SAMPLE >> fuzz_samples/fuzz.exe
    docker cp fuzz_samples/fuzz.exe $container_ID:/tmp/
    docker exec -ti $container_ID sh -c "floss -s /tmp/fuzz.exe" >> /dev/null
    docker exec -ti $container_ID sh -c "ls /tmp/"
    #docker exec -ti $container_ID sh -c "floss -s /usr/bin/zipinfo"
    done

docker stop $container_ID